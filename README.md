# Spigot

##Build
```
docker build -t minecraft:1.20.4 --build-arg="SPIGOT_VERSION=1.20.4" .
```

##Run
```
docker run -p 25565:25565/tcp minecraft:latest
```

##Dev combi
```
docker build -t spigot:dev --build-arg="SPIGOT_VERSION=1.20.4" . && docker run -it -e "MOTD=testje" spigot:dev 
```