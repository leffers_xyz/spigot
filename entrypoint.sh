#!/bin/bash

for ENTRYPOINT in $(ls /entrypoints | sort -h); do
    echo "ENTRYPOINT $ENTRYPOINT"
    bash -c "/entrypoints/$ENTRYPOINT" || { echo "/entrypoints/$ENTRYPOINT failed"; exit 1; }
done

# Execute command
exec "$@"