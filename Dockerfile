FROM alpine:latest AS openjdk
ARG OPENJDK=https://aka.ms/download-jdk/microsoft-jdk-17.0.9-alpine-x64.tar.gz

# Download OpenJDK
RUN wget -O openjdk.tar.gz $OPENJDK

# Extract OpenJDK to /opt
RUN mkdir /opt/java
RUN tar -xf openjdk.tar.gz -C /var/tmp/
RUN cp -r /var/tmp/jdk*/* /opt/java/

FROM alpine:latest AS java
# Copy java home
COPY --from=openjdk /opt/java /opt/java
# Add java to PATH
ENV PATH="$PATH:/opt/java/bin"


FROM java as build
ARG SPIGOT=https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
ARG SPIGOT_VERSION=1.19.4

# Download Spigot BuildTools
RUN wget -O /opt/BuildTools.jar $SPIGOT

# Build Spigot
RUN apk add git
# RUN git config --global --unset core.autocrlf
WORKDIR /opt
RUN java -jar /opt/BuildTools.jar --rev $SPIGOT_VERSION

# Move and rename Spigot jar
RUN mkdir /opt/minecraft
RUN cp /opt/spigot-$SPIGOT_VERSION.jar /opt/minecraft/spigot.jar


FROM java AS runtime
COPY --from=build /opt/minecraft /opt/minecraft

# Define volumes
VOLUME ["/opt/minecraft/world", "/opt/minecraft/world_nether", "/opt/minecraft/world_the_end", "/opt/minecraft/plugins"]

# Common dependencies
RUN apk add bash screen

COPY entrypoints /entrypoints
COPY entrypoint.sh /opt/entrypoint.sh

WORKDIR /opt/minecraft
ENV MIN_MEM=1G
ENV MAX_MEM=2G
ENTRYPOINT ["/opt/entrypoint.sh"]