#!/bin/bash

# Set server.properties file values
grep -v "#" server.properties | while read PROPERTY; 
do
    # Properties key from properties file
    PROPERTY_KEY=$(echo $PROPERTY | awk -F '=' '{ print $1 }')

    # Name of env var that holds value to overwrite PROPERTY_KEY's value with
    ENV_VAR_NAME=$(echo $PROPERTY_KEY | tr '[:lower:]' '[:upper:]' | sed 's/-/_/g' | sed 's/\./_/g')

    # If env var is set, replace in properties file
    if [ -n "${!ENV_VAR_NAME}" ]; then
        sed -i "s/^$PROPERTY_KEY=.*/$PROPERTY_KEY=$(eval "echo \$$ENV_VAR_NAME")/" "server.properties"
    fi

done