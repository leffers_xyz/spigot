#!/bin/bash

#Have minecraft server files been generated?
[ -f "server.properties" ] && { echo "Minecraft server files have already been generated"; exit 0; }

# Generate default files and stop server
java -Xms1G -Xmx1G -XX:+UseG1GC -jar spigot.jar nogui & sleep 5 && kill -9 $(pgrep -f spigot.jar) 2&>/dev/null