#!/bin/bash

# Run server
screen -S spigot -dm java -Xms$MIN_MEM -Xmx$MAX_MEM -XX:+UseG1GC -jar spigot.jar nogui

# Keep container running
tail -f /dev/null